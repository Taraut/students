const path = require('path');
const express = require('express');
// const endpApiGateway = require('./endp/endp_api_gateway');

const app = express();

// eslint-disable-next-line no-undef
const publicDirectoryPath = path.join(__dirname, './');
app.use(express.static(publicDirectoryPath));

// view engine setup
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, '/'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.get('/', function (req, res) {
    res.render('index');
});


/////////////////////////////////////
// API gateway section

// app.get('/prodlist', function (req, res) {
//     endpApiGateway(req, res);
// });

// END OF API gateway section
/////////////////////////////////////

// ill 404 emulator
// app.get("*", function (req, res) {
//     res.render('index');
// });

module.exports = app;
