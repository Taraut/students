import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
    Form,
    Input,
    DatePicker,
    Checkbox,
    Table,
    Dropdown,
    Menu,
    Button,
} from 'antd';
import { TextTitle } from '../atoms/textTitle';
import { ButtonC } from '../atoms/buttonC';
import { mediaData } from '../defaults';

var studentsList = [];
var addStudentDisable = true;
var nameValid = false;
var dateSelected = false;
var classSelected = false;

const Students = (props) => {
    var name,
        classes,
        addStudent,
        students,
        fillField,
        enrlDate,
        classOptions = [],
        deleteSt,
        showAll,
        filterClass;

    if (props.texts && props.texts.students) {
        ({
            name,
            enrlDate,
            classes,
            addStudent,
            students,
            fillField,
            classOptions = [],
            deleteSt,
            showAll,
            filterClass,
        } = props.texts.students);
    }

    var windowWidth = 887;
    props.windowSize && props.windowSize.width
        ? (windowWidth = props.windowSize.width)
        : (windowWidth = 888);

    var formLayout = 'vertical';
    if (windowWidth > mediaData.tabletMaxWidth) {
        formLayout = 'inline';
    }

    const buttonAdd = {
        type: 'primary',
        htmlType: 'submit',
        size: 'large',
    };

    const [form] = Form.useForm();

    const [studentsTable, setStudentsTable] = useState([]);

    const onFinish = (values) => {
        const newStudent = {
            studentName: values.nameInput,
            studentEnrlDate: values.enrlDatePicker,
            studentClasses: values.classesCheckbox,
        };

        studentsList.push(newStudent);

        addStudentDisable = true;
        nameValid = false;
        dateSelected = false;
        classSelected = false;

        form.resetFields();
        setStudentsTable(showStudentTable(studentsList));
    };

    const onClassFilterClick = (event) => {
        const key = Math.trunc(event.key);

        if (key == 0) {
            setStudentsTable(showStudentTable(studentsList));
            return;
        }

        var filteredStudentList = [];
        studentsList.forEach((element) => {
            if (element.studentClasses.includes(key)) {
                filteredStudentList.push(element);
            }
        });

        const studentsListCopy = studentsList;
        studentsList = filteredStudentList;
        setStudentsTable(showStudentTable(studentsList));
        studentsList = studentsListCopy;
    };

    ///////////////////////////////
    // student table
    const showStudentTable = () => {
        var dataSource = [];
        var columns = [
            {
                title: name,
                dataIndex: 'studentName',
                key: 'studentName',
                defaultSortOrder: 'descend',
                sorter: (a, b) => a.studentName.localeCompare(b.studentName),
            },
            {
                title: enrlDate,
                dataIndex: 'studentEnrlDate',
                key: 'studentEnrlDate',
                defaultSortOrder: 'descend',
                sorter: (a, b) =>
                    moment(a.studentEnrlDate).unix() -
                    moment(b.studentEnrlDate).unix(),
            },
            {
                title: classes,
                dataIndex: 'studentClasses',
                key: 'studentClasses',
            },
            {
                title: '',
                dataIndex: 'deleteAction',
                key: 'deleteAction',
            },
        ];

        const classesParser = (classesCode) => {
            var classesByName = [];
            classesCode.forEach((classElement) => {
                const className = classOptions.find(
                    ({ value }) => value === classElement
                );
                classesByName.push(className.label + ' ');
            });
            return classesByName;
        };

        const deleteButton = (element) => {
            const deleteStOnClick = () => {
                studentsList = studentsList.filter((item) => item !== element);

                setStudentsTable(showStudentTable(studentsList));
            };

            const buttonStudentDelete = {
                size: 'small',
            };
            return (
                <ButtonC
                    text={deleteSt}
                    onClick={deleteStOnClick}
                    data={buttonStudentDelete}
                />
            );
        };

        var keyUnique = 1;
        studentsList.forEach((element) => {
            dataSource.push({
                key: keyUnique++,
                studentName: element.studentName,
                studentEnrlDate: element.studentEnrlDate
                    .toISOString()
                    .split('T')[0],
                studentClasses: classesParser(element.studentClasses),
                deleteAction: deleteButton(element),
            });
        });

        // student table filter
        const menuItems = classOptions.map((element) => {
            return <Menu.Item key={element.value}>{element.label}</Menu.Item>;
        });
        const menuFilter = () => {
            return (
                <Menu onClick={onClassFilterClick}>
                    <Menu.Item key="0">{showAll}</Menu.Item>
                    {menuItems}
                </Menu>
            );
        };

        return (
            <div>
                <Dropdown overlay={menuFilter()} placement="topLeft">
                    <Button>{filterClass}</Button>
                </Dropdown>

                <Table dataSource={dataSource} columns={columns} />
            </div>
        );
    };

    // table translation implementation
    useEffect(() => {
        setStudentsTable(showStudentTable(studentsList));
    }, [classOptions]);

    ///////////////////////////
    // validate NAME
    const [nameInput, setName] = useState({
        value: '',
    });

    const validateChangedName = (nameInput) => {
        var nameDef = nameInput.nativeEvent.srcElement.defaultValue;
        var nameNat = nameInput.nativeEvent.data;

        if (!nameDef) nameDef = '';

        if (nameNat == null) {
            nameDef = nameDef.slice(0, -1);
            nameNat = '';
        }

        if (nameDef.length <= 0) {
            nameValid = false;
            addButtonActivator();
            return {
                validateStatus: 'error',
                errorMsg: fillField,
            };
        }

        nameValid = true;
        addButtonActivator();
        return;
    };

    const onNameChange = (value) => {
        setName({ ...validateChangedName(value), value });
    };
    /////////////

    ///////////////////////////
    // validate enrlDatePicker

    const [enrlDatePicker, setEnrlDatePicker] = useState({
        value: '',
    });

    const validateChangedEnrlDate = (enrlDatePicker) => {
        dateSelected = false;
        if (enrlDatePicker) {
            dateSelected = true;
            addButtonActivator();
        }
        return;
    };

    const onEnrlDatePickerChange = (value) => {
        setEnrlDatePicker({ ...validateChangedEnrlDate(value), value });
    };
    ///////////////////////////

    //////////////////////////
    // validate Classes
    const [classesCheckbox, setClassesCheckbox] = useState({
        value: '',
    });

    const validateClassesCheckbox = (classesCheckbox) => {
        classSelected = false;
        if (classesCheckbox && classesCheckbox.length > 0) {
            classSelected = true;
        }
        addButtonActivator();
        return;
    };

    const onClassesCheckboxChange = (value) => {
        setClassesCheckbox({ ...validateClassesCheckbox(value), value });
    };
    //////////////////////////

    //////////////////////////
    // ADD button
    const addButtonActivator = () => {
        addStudentDisable = true;
        if (
            nameValid == true &&
            dateSelected == true &&
            classSelected == true
        ) {
            addStudentDisable = false;
        }
    };

    return (
        <div>
            {students && <TextTitle text={students} />}
            <div className="students_form students_wrapper">
                <Form
                    form={form}
                    name="studentsForm"
                    onFinish={onFinish}
                    layout={formLayout}
                >
                    <Form.Item
                        name="nameInput"
                        rules={[{ required: true, message: fillField }]}
                        validateStatus={nameInput.validateStatus}
                        help={nameInput.errorMsg}
                    >
                        <Input
                            size="large"
                            id="inputName"
                            autoComplete="off"
                            allowClear="true"
                            placeholder={name}
                            value={nameInput.value}
                            onChange={onNameChange}
                        />
                    </Form.Item>

                    <Form.Item
                        name="enrlDatePicker"
                        rules={[{ required: true, message: fillField }]}
                        validateStatus={enrlDatePicker.validateStatus}
                        help={enrlDatePicker.errorMsg}
                    >
                        <DatePicker
                            id="datePickerEnrl"
                            placeholder={enrlDate}
                            value={enrlDatePicker.value}
                            onChange={onEnrlDatePickerChange}
                        />
                    </Form.Item>

                    <Form.Item
                        name="classesCheckbox"
                        rules={[{ required: true, message: fillField }]}
                        validateStatus={enrlDatePicker.validateStatus}
                        help={enrlDatePicker.errorMsg}
                    >
                        <Checkbox.Group
                            id="datePickerEnrl"
                            placeholder={classes}
                            options={classOptions}
                            value={classesCheckbox.value}
                            onChange={onClassesCheckboxChange}
                        />
                    </Form.Item>

                    <Form.Item shouldUpdate={true}>
                        {() => {
                            return (
                                <ButtonC
                                    text={addStudent}
                                    data={buttonAdd}
                                    disabled={addStudentDisable}
                                />
                            );
                        }}
                    </Form.Item>
                </Form>
            </div>

            {studentsTable && <div>{studentsTable}</div>}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        texts: state.itemsReducer.texts,
        windowSize: state.itemsReducer.windowSize,
    };
};

export default connect(mapStateToProps)(Students);
