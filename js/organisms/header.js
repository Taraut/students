import React from 'react';
import LanguageSelector from '../molecules/languageSelector';
import { connect } from 'react-redux';
import { getWindowSize } from '../redux/actions';
import { mediaData } from '../defaults';
import { TextSubtitle } from '../atoms/TextSubtitle';
import { translation } from '../translations/eng';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ws: {
                width: 555,
                height: 666,
            },
            headerTitle: translation.headerTitle,
        };

        this.windowSizeProcess = this.windowSizeProcess.bind(this);
    }

    componentDidMount() {
        this.windowSizeProcess();
        window.addEventListener('resize', this.windowSizeProcess);
    }

    componentWillUnmount() {
        window.addEventListener('resize', this.windowSizeProcess);
    }

    windowSizeProcess() {
        const ws = {
            width: window.innerWidth,
            height: window.innerHeight,
        };
        this.setState({
            ws: ws,
        });
        this.props.dispatch(getWindowSize({ windowSize: ws }));
    }

    render() {
        var { ws, headerTitle } = this.state;
        if (this.props.texts) headerTitle = this.props.texts.headerTitle;

        // Desktop > 1024px
        if (ws.width > mediaData.tabletMaxWidth) {
            return (
                <div className="header_wrapper">
                    <div className="header_firstLine">
                        <div className="header_image">
                            <img
                                src={'../../img/logo.png'}
                                alt="Arvato"
                                className="header_logo"
                            />
                        </div>
                        {headerTitle && <TextSubtitle text={headerTitle} />}
                        <div className="header_languageSelector">
                            <LanguageSelector />
                        </div>
                    </div>
                </div>
            );
        }

        // Tablet and Mobile <1024px
        return (
            <div className="header_wrapper">
                <div className="header_firstLine">
                    <div className="header_image">
                        <img
                            src={'../../img/logo.png'}
                            alt="Arvato"
                            className="header_logo"
                        />
                    </div>
                    <div className="header_languageSelector">
                        <LanguageSelector />
                    </div>
                </div>
                {headerTitle && <TextSubtitle text={headerTitle} />}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        windowSize: state.itemsReducer.windowSize,
        texts: state.itemsReducer.texts,
    };
};

export default connect(mapStateToProps)(Header);
