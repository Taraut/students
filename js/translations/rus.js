exports.translation = {
    lang: 'Русский',
    headerTitle: 'ПРОВЕРОЧНОЕ ЗАДАНИЕ',
    students: {
        students: 'Студенты',
        name: 'Имя: ',
        enrlDate: 'Дата зачисления: ',
        classes: 'Классы: ',
        addStudent: 'Добавить',
        fillField: 'Пожалуйста заолните поле',
        classOptions: [
            {label: 'Музыка', value: 1},
            {label: 'Живопись', value: 2},
            {label: 'Танцы', value: 3},
        ],
        showAll: 'Показывать все',
        deleteSt: 'Удалить',
        filterClass: 'Выбрать класс',
    },
};
