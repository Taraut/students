exports.translation = {
    lang: 'English',
    headerTitle: 'RECRUITMENT ASSIGNMENT',
    students: {
        students: 'Students',
        name: 'Name: ',
        enrlDate: 'Enrollment Date: ',
        classes: 'Classes: ',
        addStudent: 'Add Student',
        fillField: 'Please fill in this field',
        classOptions: [
            {label: 'Music', value: 1},
            {label: 'Painting', value: 2},
            {label: 'Dancing', value: 3},
        ],
        showAll: 'Show All',
        deleteSt: 'Delete',
        filterClass: 'Select class',
    },
};
