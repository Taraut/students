import React from 'react';
import { Spin } from 'antd';

const SpinC = () => {
    return (
        <div className="spinc_wrapper">
            <Spin size="large" />
        </div>
    );
};

export { SpinC };
