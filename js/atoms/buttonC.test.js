import React from 'react';
import TestRenderer from 'react-test-renderer';
import { ButtonC } from './buttonC';

const testRenderer = TestRenderer.create(
    <ButtonC text="Test Text"  />
);
const testInstance = testRenderer.root;

test('ButtonC has text', () => {
    expect(testInstance.findByProps({ className: 'ButtonC_text' }).children).toEqual([
        'Test Text',
    ]);
});
