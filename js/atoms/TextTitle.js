import React from 'react';
import PropTypes from 'prop-types';

const TextTitle = ({ text }) => {
    return <div className="text_title">{text}</div>;
};

TextTitle.propTypes = {
    text: PropTypes.string,
};
TextTitle.defaultProps = {
    text: '',
};

export { TextTitle };
