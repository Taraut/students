import React, { Suspense, lazy } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Route, Switch } from 'react-router-dom';
import { Row, Col } from 'antd';
import { SpinC } from './atoms/SpinC';

import configureStore, { history } from './store/configureStore';

const store = configureStore();

const Header = lazy(() => import('./organisms/Header'));
const Students = lazy(() => import('./organisms/Students'));

const jsx = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Suspense fallback={<SpinC />}>

                <Row>
                    <Col span={2} />
                    <Col span={20}>
                        <Header />
                    </Col>
                    <Col span={2} />
                </Row>

                <Row>
                    <Col span={2} />
                    <Col span={20} className="mainPart">
                        <nav>
                            <Switch>
                                <Route exact path="/" component={Students} />
                                <Route path="/Students" component={Students} />
                            </Switch>
                        </nav>
                    </Col>
                    <Col span={2} />
                </Row>
            </Suspense>
        </ConnectedRouter>
    </Provider>
);

render(jsx, document.getElementById('root'));
