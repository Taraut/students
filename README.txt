Allen Joelson
Riga, XX XXXX 2021

                    RECRUITMENT ASSIGNMENT FOR INTERMEDIATE FRONT-END DEVELOPER
                    ------------------------------------------------------------------

                    CONTENTS

PREFACE

SECURITY WARNING

CHAPTER 1. INTRODUCTION
Section I. Software Overview
            1-1. Structure

CHAPTER 2. TECHNICAL IMPLEMENTATION
Section I. Web Server
            2-1. General Information
            2-2. Server Start
            2-3. Defaults Change
Section II. Client Part
            2-6. General Information
            2-7. Default Url
            2-8. Server Start
            2-9. css-styling
            2-10. Run Mode
            2-11. Multi Language Support
            2-12. Jest Unit Tests Start

Section III. Availability


                    PREFACE

This manual provides guidance for RECRUITMENT ASSIGNMENT FOR INTERMEDIATE FRONT-END DEVELOPER (RAJIFED).
You may also contact for more information:
Email allen.joelson@gmail.com
Phone (371) 295-37455


                    SECURITY WARNING

Some UI elements use China made JavaScript library (https://ant.design/).
Also, no any security check for other parts of this software is done.
Check your policies before copy or run RAJIFED software on devices with sensitive information.


                    CHAPTER 1.
                    INTRODUCTION

                    Section I. SOFTWARE OVERVIEW

1-1.	STRUCTURE

RAJIFED consist of 2 separate independent parts:
1.	A Web Server (WS)
2.	A Client Part (CP)


                    CHAPTER 2.
                    TECHNICAL IMPLEMENTATION

                    Section I. WEB SERVER

2-1. GENERAL INFORMATION

WS part runs on Node.js.


2-2. SERVER START

WS utilize environment variables (.env) and must starting with preloading.
Starting command: node -r dotenv/config server.js


2-3. DEFAULTS CHANGE

Default parameters change could be easy done in server.js file in special section.


                    Section II. CLIENT PART


2-6. GENERAL INFORMATION

CP frontend based on React JavaScript library.


2-7. DEFAULT URL

url: 'http://127.0.0.1:3000'
Use of url: 'http://localhost:3000' is not recommended due to CORS policies.


2-8. SERVER START

WS utilize environment variables (.env) and must be starting with preloading.
Starting command: node -r dotenv/config server.js
Starting w/o backend microservice support (not recommended): http-server -c-1
Use  npm run build command for frontend part generation (in /publicdist folder).


2-9. CSS-STYLING

For less to css conversion use:
lessc css\css.less css\css.css
from root.


2-10. RUN MODE

By default webpack and index.html configurations are set to development mode.


2-11. MULTI LANGUAGE SUPPORT

Code structure allow easy add languages and change texts.
All same language texts are in one separate file (/translations folder).
"Classes" information also located in translation files.
classOptions: [
            {label: 'Music', value: 1},
            ...
            {label: 'Dancing', value: 3},
        ],
Where value number represents 'unique' class id.
Qtt. of classes are not limited and can be easyly added or removed ('active' status could be added by request).


2-12. JEST UNIT TESTS START
For to start Jest unit tests execute: npm test.

                    Section III. AVAILABILITY

RAJIFED software is available from GitLab repository:
https://gitlab.com/arvato.systems.latvia/development/frontend/allen-joeloson#




